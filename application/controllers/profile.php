<?php
class Profile extends Controller 
{
	function __construct()
	{
		parent::__construct('profile');
	}
	
	function index($customer_id=11)
	{
		$data['person_info']=$this->Customer->get_info($customer_id);
		$data['all_ministries']=$this->Module->get_all_ministries();

		$this->load->view('mobile/profile',$data);
	}
		
	/*
	Inserts/updates a customer
	*/
	function save($customer_id=-1)
	{
		$person_data = array(
		'first_name'=>$this->input->post('first_name'),
		'last_name'=>$this->input->post('last_name'),
		'gender'=>$this->input->post('gender'),
		'phone_number'=>$this->input->post('phone_number'),
		'email'=>$this->input->post('email'),
		'estate'=>$this->input->post('estate')
		);
		$customer_data=array(
		'username'=>$this->input->post('username'),
		'password'=>md5($this->input->post('password'))
		);
		$ministry_data = $this->input->post("ministries")!=false ? $this->input->post("ministries"):array();
		$permission_data = "profile";
		if($this->Customer->save($person_data,$customer_data,$ministry_data,$permission_data,$customer_id))
		{
			//New customer
			if($customer_id==-1)
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('customers_successful_adding').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_data['person_id']));
			}
			else //previous customer
			{
				echo json_encode(array('success'=>true,'message'=>$this->lang->line('customers_successful_updating').' '.
				$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>$customer_id));
			}
		}
		else//failure
		{	
			echo json_encode(array('success'=>false,'message'=>$this->lang->line('customers_error_adding_updating').' '.
			$person_data['first_name'].' '.$person_data['last_name'],'person_id'=>-1));
		}
		$this->index();
	}
	
}
?>