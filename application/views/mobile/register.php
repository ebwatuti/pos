<!DOCTYPE html>
<html>
<head>
    <title>Sign Up</title>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/bootstrap-theme.min.css" />
    <script src="<?php echo base_url();?>js/bootstrap.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <style type="text/css">
    body { padding-top:30px; }
    .form-control { margin-bottom: 10px; }
    .row legend { text-align: center;}
</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 well well-sm">
            <legend><a href="http://www.icc-kenya.org"><i class="glyphicon glyphicon-globe"></i></a> Frontrunnerz Sign Up!</legend>
            <?php
echo form_open('register/save/'.$person_info->person_id,array('id'=>'customer_form', 'class'=>'form-horizontal'));
?>
            <div class="row">
                <div class="col-xs-6 col-md-6">
                    <?php echo form_input(array(
                        'name'=>'first_name',
                        'id'=>'first_name',
                        'class'=>'form-control',
                        'placeholder'=>'First Name',
                        'required'=>'required',
                        'value'=>$person_info->first_name)
                    );?>
                </div>
                <div class="col-xs-6 col-md-6">
                    <?php echo form_input(array(
                        'name'=>'last_name',
                        'id'=>'last_name',
                        'class'=>'form-control',
                        'placeholder'=>'Last Name',
                        'required'=>'required',
                        'value'=>$person_info->last_name)
                    );?>
                </div>
            </div>
                    <?php echo form_input(array(
                        'type'=>'email',
                        'name'=>'email',
                        'id'=>'email',
                        'class'=>'form-control',
                        'placeholder'=>'Your Email',
                        'required'=>'required',
                        'value'=>$person_info->email)
                    );?>

                    <?php echo form_label('Gender ', 'gender'); ?>
                    <?php echo form_dropdown('gender',array(
                        'Male'=>'Male',
                        'Female'=>'Female'),
                        'class="form-control"',
                        $person_info->gender  
                    );?>

                    <?php echo form_input(array(
                        'name'=>'phone_number',
                        'id'=>'phone_number',
                        'class'=>'form-control',
                        'placeholder'=>'Your Mobile',
                        'required'=>'required',
                        'value'=>$person_info->phone_number)
                    );?>

                    <?php echo form_input(array(
                        'name'=>'estate',
                        'id'=>'estate',
                        'class'=>'form-control',
                        'placeholder'=>'Your Estate',
                        'required'=>'required',
                        'value'=>$person_info->estate)
                    );?>

                    <?php echo form_label('Ministry ', 'ministry'); ?>
                    <ul id="ministry_list">
                    <?php
                        foreach($all_ministries->result() as $ministry)
                        {
                    ?>
                    <li>    
                    <?php echo form_checkbox("ministries[]",$ministry->ministry_name); ?>
                    <span class="medium"><?php echo "$ministry->ministry_name";?></span>
                    </li>
                    <?php
                    }
                    ?>
                    </ul>

                    <?php echo form_input(array(
                        'name'=>'username',
                        'id'=>'username',
                        'class'=>'form-control',
                        'placeholder'=>'Username',
                        'required'=>'required',
                        'value'=>$person_info->username)
                    );?>

                    <?php echo form_input(array(
                        'type'=>'password',
                        'name'=>'password',
                        'id'=>'password',
                        'class'=>'form-control',
                        'placeholder'=>'Password',
                        'required'=>'required',
                        'value'=>$person_info->password)
                    );?>
            <br />
            <br />

            <button class="btn btn-lg btn-primary btn-block" type="submit">
                Sign up</button>
            <?php 
echo form_close();
?>
        </div>
    </div>
</div>
</body>
</html>


