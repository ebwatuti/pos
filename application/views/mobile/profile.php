<!DOCTYPE html>
<html>

<head>
    <title>Profile</title>
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css" />
    <link rel="stylesheet" rev="stylesheet" href="<?php echo base_url();?>css/bootstrap-theme.min.css" />
    <script src="<?php echo base_url();?>js/jquery-1.11.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js" type="text/javascript" language="javascript" charset="UTF-8"></script>
    <style type="text/css">
    .form-control { margin-bottom: 10px; }
    .bs-example {
        margin: 20px;}
    .row legend { text-align: center;}
    </style>
    <script type="text/javascript">
    var $template = $(".template");

    var hash = 2;
    $(".btn-add-panel").on("click", function() {
        var $newPanel = $template.clone();
        $newPanel.find(".collapse").removeClass("in");
        $newPanel.find(".accordion-toggle").attr("href", "#" + (++hash))
            .text("Dynamic panel #" + hash);
        $newPanel.find(".panel-collapse").attr("id", hash).addClass("collapse").removeClass("in");
        $("#accordion").append($newPanel.fadeIn());
    });
    </script>
</head>

<body>
    <div class="container">
    <div class="row">
        <legend><a href="http://www.icc-kenya.org"><i class="glyphicon glyphicon-globe"></i></a> User Profile!</legend>
            <?php
echo form_open('profile/save/'.$person_info->person_id,array('id'=>'customer_form', 'class'=>'form-horizontal'));
?>
        <div class="bs-example">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="glyphicon glyphicon-user"> <b>Biodata</b> </a> 
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                    <?php echo form_input(array(
                        'name'=>'first_name',
                        'id'=>'first_name',
                        'class'=>'form-control',
                        'placeholder'=>'First Name',
                        'required'=>'required',
                        'value'=>$person_info->first_name)
                    );?>

                    <?php echo form_input(array(
                        'name'=>'last_name',
                        'id'=>'last_name',
                        'class'=>'form-control',
                        'placeholder'=>'Last Name',
                        'required'=>'required',
                        'value'=>$person_info->last_name)
                    );?>

                    <?php echo form_label('Gender ', 'gender'); ?>
                    <?php echo form_dropdown('gender',array(
                        'Male'=>'Male',
                        'Female'=>'Female'),
                        'class="form-control"',
                        $person_info->gender  
                    );?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="glyphicon glyphicon-envelope"> <b>Contact</b> </a> 
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                    <?php echo form_input(array(
                        'name'=>'phone_number',
                        'id'=>'phone_number',
                        'class'=>'form-control',
                        'placeholder'=>'Your Mobile',
                        'required'=>'required',
                        'value'=>$person_info->phone_number)
                    );?>

                    <?php echo form_input(array(
                        'type'=>'email',
                        'name'=>'email',
                        'id'=>'email',
                        'class'=>'form-control',
                        'placeholder'=>'Your Email',
                        'required'=>'required',
                        'value'=>$person_info->email)
                    );?>

                    <?php echo form_input(array(
                        'name'=>'estate',
                        'id'=>'estate',
                        'class'=>'form-control',
                        'placeholder'=>'Your Estate',
                        'required'=>'required',
                        'value'=>$person_info->estate)
                    );?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="glyphicon glyphicon-lock"> <b>Credentials</b> </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                    <?php echo form_input(array(
                        'name'=>'username',
                        'id'=>'username',
                        'class'=>'form-control',
                        'placeholder'=>'Username',
                        'required'=>'required',
                        'value'=>$person_info->username)
                    );?>

                    <?php echo form_input(array(
                        'type'=>'password',
                        'name'=>'password',
                        'id'=>'password',
                        'class'=>'form-control',
                        'placeholder'=>'Password',
                        'required'=>'required',
                        'value'=>$person_info->password)
                    );?>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="glyphicon glyphicon-heart"> <b>Ministry</b> </a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul id="ministry_list">
                        <?php
                            foreach($all_ministries->result() as $ministry)
                            {
                             ?>
                            <li>    
                        <?php echo form_checkbox("ministries[]",$ministry->ministry_name,$this->Customer->registered_ministry($ministry->ministry_name,$person_info->person_id)); ?>
                        <span class="medium"><?php echo "$ministry->ministry_name";?></span>
                             </li>
                        <?php
                            }
                        ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-lg btn-success btn-block" type="submit">
                Update</button>
         <?php 
echo form_close();
?>
    <br>
    <button class="btn btn-lg btn-warning btn-block" type="submit">
                Logout</button>
    </div>

</body>

</html>
